import Step from './steps/Step';

export default class Stepper {
    private state: any;
    private steps: Step[] = [];

    // copy ctor
    private constructor(steps: Step[]) {
        this.steps = steps;
    }

    //                                          what the what??
    public static addStep<T extends Step>( step: { new(): T} ): Stepper {
        const s = new step();
        return new Stepper([s]);
    }

    //                                   what the what??
    public addStep<T extends Step>( step: { new(): T} ): Stepper {
        this.steps.push(new step());
        return this;
    }

    public go(): void {
        this.steps.forEach((step) => {
            console.log("STATE: ", this.state);
            this.state = step.action(this.state);
        });
        console.log("STATE: ", this.state);
    }
}