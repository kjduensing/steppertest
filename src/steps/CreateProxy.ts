import Step from './Step';

export default class CreateProxy implements Step {
    public action(state: any) {
        const newState = {...state};

        // TODO: Consider using redux for this; possibly too complex?
        newState.proxyInfo = {
            name: 'MyProxy',
            id: '123',
            url: 'www.garfieldcat.com',
        }

        return newState;
    }
}