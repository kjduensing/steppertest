import Step from './Step';

export default class AddSecurityPolicy implements Step {
    public action(state: any) {
        const newState = {...state};

        newState.secInfo = Buffer.from(`${state.proxyInfo.name}:${state.proxyInfo.id}`).toString('base64');

        return newState;
    }
}