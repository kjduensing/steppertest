import CreateProxy from './steps/CreateProxy';
import AddSecurityPolicy from './steps/AddSecurityPolicy';
import Stepper from './Stepper';

Stepper.addStep(CreateProxy).addStep(AddSecurityPolicy).go();
